package az.Task1_Student_task.Task1_Student_task.repository;

import az.Task1_Student_task.Task1_Student_task.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<StudentEntity,Long> {
}
