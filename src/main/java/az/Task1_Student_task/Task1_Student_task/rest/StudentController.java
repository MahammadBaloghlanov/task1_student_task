package az.Task1_Student_task.Task1_Student_task.rest;

import az.Task1_Student_task.Task1_Student_task.dto.StudentDto;
import az.Task1_Student_task.Task1_Student_task.dto.StudentRequestDto;
import az.Task1_Student_task.Task1_Student_task.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
@Slf4j
@RestController
@RequestMapping("students")
@RequiredArgsConstructor
public class StudentController {


    private final StudentService studentService;

    @GetMapping("/{id}")
    public StudentDto getStudentById(@PathVariable Long id){
        return studentService.getStudentWithId(id);
    }

    @PostMapping
    public StudentDto createStudent(@PathVariable StudentRequestDto studentRequestDto){
        return studentService.create(studentRequestDto);
    }

    @PutMapping("/{id}")
    public StudentDto updateStudent(@PathVariable Long id , @PathVariable StudentRequestDto studentRequestDto){
        return studentService.update(id,studentRequestDto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id){
        studentService.delete(id);
    }

    @GetMapping
    public Page<StudentDto> getStudentList( Pageable page){
        log.info("Received page{} size{}", page);
        return studentService.list(page);
    }
}
