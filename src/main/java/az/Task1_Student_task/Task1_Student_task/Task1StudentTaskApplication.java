package az.Task1_Student_task.Task1_Student_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task1StudentTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task1StudentTaskApplication.class, args);
	}

}
