package az.Task1_Student_task.Task1_Student_task.service;

import az.Task1_Student_task.Task1_Student_task.dto.StudentDto;
import az.Task1_Student_task.Task1_Student_task.dto.StudentRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StudentService {
    StudentDto getStudentWithId(Long id);

    StudentDto create(StudentRequestDto studentRequestDto);

    StudentDto update(Long id, StudentRequestDto studentRequestDto);

    void delete(Long id);

    Page<StudentDto> list(Pageable page);
}
