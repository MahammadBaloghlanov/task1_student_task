package az.Task1_Student_task.Task1_Student_task.service;

import az.Task1_Student_task.Task1_Student_task.dto.StudentDto;
import az.Task1_Student_task.Task1_Student_task.dto.StudentRequestDto;
import az.Task1_Student_task.Task1_Student_task.entity.StudentEntity;
import az.Task1_Student_task.Task1_Student_task.exception.NotFoundException;
import az.Task1_Student_task.Task1_Student_task.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    @Override
    public StudentDto getStudentWithId(Long id) {
        final StudentEntity studentEntity = studentRepository.findById(id).orElseThrow(NotFoundException::new);
        return StudentDto.builder()
                .id(studentEntity.getId())
                .name(studentEntity.getName())
                .surname(studentEntity.getSurname())
                .age(studentEntity.getAge())
                .build();
    }

    @Override
    public StudentDto create(StudentRequestDto studentRequestDto) {
        StudentEntity studentEntity = StudentEntity.builder()
                .name(studentRequestDto.getName())
                .surname(studentRequestDto.getSurname())
                .age(studentRequestDto.getAge())
                .build();
        final StudentEntity save = studentRepository.save(studentEntity);
        return StudentDto.builder()
                .id(save.getId())
                .name(save.getName())
                .surname(save.getSurname())
                .age(save.getAge())
                .build();
    }

    @Override
    public StudentDto update(Long id, StudentRequestDto studentRequestDto) {
        StudentEntity studentEntity = StudentEntity.builder()
                .id(id)
                .name(studentRequestDto.getName())
                .surname(studentRequestDto.getSurname())
                .age(studentRequestDto.getAge())
                .build();
        final StudentEntity save = studentRepository.save(studentEntity);
        return StudentDto.builder()
                .id(save.getId())
                .name(save.getName())
                .surname(save.getSurname())
                .age(save.getAge())
                .build();
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public Page<StudentDto> list(Pageable page) {
        return studentRepository.findAll(page)
                .map(studentEntity -> StudentDto.builder()
                        .id(studentEntity.getId())
                        .name(studentEntity.getName())
                        .surname(studentEntity.getSurname())
                        .age(studentEntity.getAge())
                        .build());
    }
}
