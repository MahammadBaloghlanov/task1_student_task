package az.Task1_Student_task.Task1_Student_task.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentDto {

    private Long id;
    private String name;
    private String surname;
    private Integer age;


}
